# README #

Multi Period Assignment problem reference instances. The name of the Instance is Co_I_T_n, where I indicates the number of cells considered, T the number of time periods considered and n is the numeric identification of the instance.

### Format ###

nCell nTimeSteps nCustomerTypes

number of tasks performed by each customer type

costumerType Timestep
cost matrices

task to be performed

costumerType Timestep
number of costumer available